import Link from 'next/link';
import { useState ,useEffect, useRef } from 'react';
import { useRouter } from "next/router";
import Image from 'next/image';
import logo from '../../../public/buzzencer_logo_white.svg'

function HeaderComponent({children}) {
  // const [showMe, setShowMe] = useState(false);
  // function toggle(){
  //   setShowMe(!showMe);
  // }
  const [showDropdown, setShowDropdown] = useState(false);
  const [showNotifiedDropdown, setNotificationDropdown] = useState(false);

  // create a React ref for the dropdown element
  const dropdown = useRef(null);

  const router = useRouter();

//  user dropdown
  useEffect(() => {
    // only add the event listener when the dropdown is opened
    if (!showDropdown) return;
    function handleClick(event) {
      if (dropdown.current && !dropdown.current.contains(event.target)) {
        setShowDropdown(false);
      }
    }
    window.addEventListener("click", handleClick);
    // clean up
    return () => window.removeEventListener("click", handleClick);
  }, [showDropdown]);

// Notification dropdown
  useEffect(() => {
    // only add the event listener when the dropdown is opened
    if (!showNotifiedDropdown) return;
    function handleClick(event) {
      if (dropdown.current && !dropdown.current.contains(event.target)) {
        setNotificationDropdown(false);
      }
    }
    window.addEventListener("click", handleClick);
    // clean up
    return () => window.removeEventListener("click", handleClick);
  }, [showNotifiedDropdown]);

  // Open menu
  const [isOpen,setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen)


  return (
    <div>
      <nav className="flex fixed w-full items-center px-4 h-16 text-gray-700 z-10 bs-sidebar bg-white">
        <div className="flex items-center w-1/5">
          {/* toggle button code */}
          <button className="mr-2" aria-label="Open Menu">
            <svg
              fill="none"
              stroke="currentColor"
              strokelinecap="round"
              strokelinejoin="round"
              strokeWidth="2"
              viewBox="0 0 24 24"
              className="w-8 h-8"
            >
              <path d="M4 6h16M4 12h16M4 18h16"></path>
            </svg>
          </button>
          <Image src={logo} alt="Logo" className="h-auto w-24" />
        </div>
        <div className="flex items-start w-2/5">
          <h2 className="sec-title text-black font-medium text-base">Dashboard</h2>
        </div>
        <div className="flex items-end justify-end w-2/5">
          <div className="notification mr-5 cursor-pointer" onClick={() => setNotificationDropdown(a => !a)}>
          {showNotifiedDropdown && (
              <div ref={dropdown} className="absolute z-10 right-5 mt-2 rounded-xl shadow p-4 bg-white overflow-y-scroll notification-bx">
                  <div className="flex flex-row items-center mb-3">
                    <div className="mr-28">
                      <h3 className="font-bold text-black text-xl">Notifications</h3>
                    </div>
                    <div>
                      <Link href="/notification" passHref>
                          <a className="text-black font-medium capitalize">Sell all</a>
                      </Link>
                    </div>
                  </div>
                  <div className="latest-news">
                    <p className="font-bold text-black mb-3">New</p>
                    <div className="news-description mb-5">
                        <div className="grid grid-cols-2 mb-2">
                          <div className="name  flex items-center"><span className="w-2 h-2 rounded-full mr-2" style={{background:'#403BBF'}}></span><p className="font-bold text-black text-sm">Nike</p></div>
                          <div className="time flex justify-end"><p className="text-black text-xs font-regular">12.00 AM</p></div>
                        </div>
                        <p className="description text-black text-xs font-regular pl-4">We have an Exciting Offers for you near to yo...</p>
                    </div>

                    <div className="news-description mb-5">
                        <div className="grid grid-cols-2 mb-2">
                          <div className="name  flex items-center"><span className="w-2 h-2 rounded-full mr-2" style={{background:'#403BBF'}}></span><p className="font-bold text-black text-sm">Adidas</p></div>
                          <div className="time flex justify-end"><p className="text-black text-xs font-regular">12.00 AM</p></div>
                        </div>
                        <p className="description text-black text-xs font-regular pl-4">We have an Exciting Offers for you near to yo...</p>
                    </div>

                    <div className="news-description mb-5">
                        <div className="grid grid-cols-2 mb-2">
                          <div className="name  flex items-center"><span className="w-2 h-2 rounded-full mr-2" style={{background:'#403BBF'}}></span><p className="font-bold text-black text-sm">Relience</p></div>
                          <div className="time flex justify-end"><p className="text-black text-xs font-regular">12.00 AM</p></div>
                        </div>
                        <p className="description text-black text-xs font-regular pl-4">We have an Exciting Offers for you near to yo...</p>
                    </div>

                    <div className="news-description mb-5">
                        <div className="grid grid-cols-2 mb-2">
                          <div className="name  flex items-center"><span className="w-2 h-2 rounded-full mr-2" style={{background:'#403BBF'}}></span><p className="font-bold text-black text-sm">Pinkvilla</p></div>
                          <div className="time flex justify-end"><p className="text-black text-xs font-regular">12.00 AM</p></div>
                        </div>
                        <p className="description text-black text-xs font-regular pl-4">We have an Exciting Offers for you near to yo...</p>
                    </div>

                  </div>

                  <div className="earlier-news">
                    <p className="font-bold text-black mb-3">Earlier</p>
                    <div className="news-description mb-5">
                        <div className="grid grid-cols-2 mb-2">
                          <div className="name  flex items-center"><p className="font-bold text-black text-sm">Nescafe</p></div>
                          <div className="time flex justify-end"><p className="text-black text-xs font-regular">12.00 AM</p></div>
                        </div>
                        <p className="description text-black text-xs font-regular">We have an Exciting Offers for you near to yo...</p>
                    </div>

                    <div className="news-description mb-5">
                        <div className="grid grid-cols-2 mb-2">
                          <div className="name  flex items-center"><p className="font-bold text-black text-sm">Samsung</p></div>
                          <div className="time flex justify-end"><p className="text-black text-xs font-regular">12.00 AM</p></div>
                        </div>
                        <p className="description text-black text-xs font-regular">We have an Exciting Offers for you near to yo...</p>
                    </div>

                    <div className="news-description mb-5">
                        <div className="grid grid-cols-2 mb-2">
                          <div className="name  flex items-center"><p className="font-bold text-black text-sm">Nescafe</p></div>
                          <div className="time flex justify-end"><p className="text-black text-xs font-regular">12.00 AM</p></div>
                        </div>
                        <p className="description text-black text-xs font-regular">We have an Exciting Offers for you near to yo...</p>
                    </div>

                    <div className="news-description mb-5">
                        <div className="grid grid-cols-2 mb-2">
                          <div className="name  flex items-center"><p className="font-bold text-black text-sm">Samsung</p></div>
                          <div className="time flex justify-end"><p className="text-black text-xs font-regular">12.00 AM</p></div>
                        </div>
                        <p className="description text-black text-xs font-regular">We have an Exciting Offers for you near to yo...</p>
                    </div>
                  </div>
              </div>
            )}
        </div>
        <div className="adminbx">
          <div className="admin-drop cursor-pointer flex items-center" onClick={() => setShowDropdown(b => !b)}>
            <div className="user-icn w-9 h-9 rounded-full relative flex justify-center items-center mr-2" style={{background:'#403BBF'}}>
              <i className="icon icon-Profile text-white text-lg"></i>
            </div>
            <span className="mr-2 text-black font-semibold">Prasad</span>
            <i className="icon icon-arrow text-black"></i>
          </div>
          {showDropdown && (
              <div ref={dropdown} className="absolute right-5 mt-2 rounded-xl shadow p-7 bg-white profile-dropdwn">
                <ul>
                  <li className="mb-4">
                      <Link href="/reset-password-admin" passHref>
                        <a className="text-black font-medium">Reset Password</a>
                      </Link>
                  </li>
                  <li>
                      <Link href="/" passHref>
                        <a className="text-black font-medium">Logout</a>
                      </Link>
                  </li>
                </ul>
              </div>
            )}
        </div>
    </div>


    <aside  className="transform top-0 left-0 2xl:w-80 w-64 fixed h-full overflow-auto ease-in-out transition-all duration-300 z-30 left-sidenav" style={{width:isOpen ? "70px":"310px"}}>
      <div className="flex w-full items-center h-15 p-4 justify-between">
        <Link href="/" passHref>
          <a><Image src={logo} alt="Logo" className="h-auto w-32  mx-auto" /></a>
        </Link>
        <button onClick={toggle} className="mr-2 2xl:hidden" aria-label="Open Menu">
        <svg
          fill="none"
          stroke="currentColor"
          strokelinecap="round"
          strokelinejoin="round"
          strokeWidth="2"
          viewBox="0 0 24 24"
          className="w-8 h-8"
        >
          <path d="M4 6h16M4 12h16M4 18h16"></path>
        </svg>
      </button>
      </div>
      {
        <>
          <div className={router.pathname == "/admindashboard" ? "active" : ""}>
            <Link href="/admindashboard" passHref>
              <a>
                <span className="flex items-center justify-left cursor-pointer links p-3 py-2.5 m-3 capitalize leading-normal"><span className="mr-5">
                      <i className="icon-Dashboard text-2xl"></i>
                  </span>
                  <span className="text-base ">Dashboard</span>
                </span>
              </a>
          </Link>
      </div>

      <div className={router.pathname == "/adminCampaign/campaign" ? "active" : ""}>
        <Link href="/adminCampaign/campaign" passHref>
          <a>
            <span
              className="flex items-center justify-left cursor-pointer links p-3 py-2.5 m-3.5 capitalize leading-normal"
              ><span className="mr-5">
                <i className="icon-Calender text-2xl"></i>
              </span>
              <span className="text-base">Create Campaign</span>
            </span>
          </a>
        </Link>
      </div>
      
      <div className={router.pathname == "/adminCampaignlist/list1" ? "active" : ""}>
        <Link href="/adminCampaignlist/list1" passHref>
          <a>
            <span
            className="flex items-center justify-left cursor-pointer links p-3 py-2.5 m-3.5 capitalize leading-normal"
            ><span className="mr-5">
              <i className="icon-list text-2xl"></i>
            </span>
            <span className="text-base">Campaign List</span>
            </span>
          </a>
        </Link>
      </div>

      <div className={router.pathname == "/adminCampaignlist/list2" ? "active" : ""}>
        <Link href="/adminCampaignlist/list2" passHref>
          <a>
            <span
            className="flex items-center justify-left cursor-pointer links p-3 py-2.5 m-3.5 capitalize leading-normal"
            ><span className="mr-5">
              <i className="icon icon-Profile text-2xl"></i>
            </span>
            <span className="text-base">User List</span>
            </span>
          </a>
        </Link>
      </div>
          </>
      }

      <main>{children}</main>
    </aside>



  </nav>
    </div>
  )
}

export default HeaderComponent



{/* 
    <transition
      enter-className="opacity-0"
      enter-active-className="ease-out transition-medium"
      enter-to-className="opacity-100"
      leave-className="opacity-100"
      leave-active-className="ease-out transition-medium"
      leave-to-className="opacity-0"
    >
      <div
        v-show="isOpen"
        className="z-10 fixed inset-0 transition-opacity"
      >
        <div
          className="absolute inset-0 bg-black opacity-50"
          tabindex="0"
        ></div>
      </div>
    </transition> */}