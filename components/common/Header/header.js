import Link from 'next/link';
import { useState } from 'react';
import Image from 'next/image';
import { useRouter } from "next/router";
import logo from '../../../public/buzzencer_logo.svg'

function HeaderComponent() {
    const router = useRouter();
    const [active, setActive] = useState(false);

    const handleClick = () => {
        setActive(!active);
    };
    return ( 
        // <div className="bs-header fixed w-full bg-transparent z-10">
        <div className="bs-header">
            <div className="container mx-auto px-4 xl:px-0 relative z-10">      
                <nav className='flex items-center flex-wrap pt-3 pb-3'>
                        <Link href='/' passHref>
                            <a>
                                <Image src={logo} alt="Buzzencer" />
                            </a>
                        </Link>
                   
                    <button  className="inline-flex rounded lg:hidden  ml-auto  outline-none cursor-pointer" onClick={handleClick}>
                        <i className="icon-list text-3xl"></i>
                    </button>
                    <div
                        className={`${
                            active ? '' : 'hidden'
                        }   w-full lg:inline-flex lg:flex-grow lg:w-auto `}
                    >
                        <div className='lg:inline-flex lg:flex-row lg:ml-auto lg:w-auto w-full lg:items-center items-center  flex flex-col lg:h-auto lg:pt-0 pt-6 nav-link-bx header_space'>
                            <div className={router.pathname == "/" ? "active" : ""}>
                                <Link href='/' passHref>
                                <a className="lg:ml-7 mb-6 lg:mb-0 lg:text-black text-white text-base font-medium header_space">
                                    Home
                                </a>
                                </Link>
                            </div>
                            <div className={router.pathname == "/aboutus" ? "active" : ""}>
                                <Link href='/' passHref>
                                    <a className="lg:ml-7 mb-6 lg:mb-0 lg:text-black text-white text-base font-medium header_space">
                                        About us
                                    </a>
                                </Link>
                            </div>
                            <div className={router.pathname == "/contact-us" ? "active" : ""}>
                                <Link href='/contact-us' passHref>
                                    <a className="lg:ml-7 mb-6 lg:mb-0 lg:text-black text-white text-base font-medium header_space">
                                        Contact
                                    </a>
                                </Link>
                            </div>
                            <div className={router.pathname == "/dashboard" ? "active" : ""}>
                                <Link href='/dashboard' passHref>
                                    <a className="lg:ml-7 mb-6 lg:mb-0 lg:text-black text-white text-base font-medium header_space">
                                        Influencer
                                    </a>
                                </Link>
                            </div>
                            <div className={router.pathname == "/admindashboard" ? "active" : ""}>
                                <Link href='/admindashboard' passHref>
                                    <a className="lg:ml-7 mb-6 lg:mb-0 lg:text-black text-white text-base font-medium header_space">
                                        Dashboard
                                    </a>
                                </Link>
                            </div>
                                <Link href='/signup' passHref>
                                    <a className="lg:ml-7 mb-6 lg:mb-0 bs-btn typ-login text-white font-semibold text-center py-3 rounded-full">Sign In</a>
                                </Link>
                        </div>
                    </div>
                </nav>
            </div> 
        </div>
    )
}
export default HeaderComponent