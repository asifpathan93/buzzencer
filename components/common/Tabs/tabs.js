import Link from 'next/link';
import Image from 'next/image';
import logo from '../../../public/buzzencer_logo.svg'
import React from 'react';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
// import 'react-tabs/style/react-tabs.css';

function TabsComponent() {    
    return (  
        <div>
            <Tabs className="bs-tab">
                <TabList className="flex justify-center md:w-2/3 w-auto mx-auto mb-8 text-center">
                    <Tab className="w-2/4 py-3 font-semibold cursor-pointer">Sign In</Tab>
                    <Tab className="w-2/4 py-3 font-semibold cursor-pointer">Sign Up</Tab>
                </TabList> 
                <TabPanel>
                    <div className="bs-form">
                        <form>
                            <div className="mb-5">
                                <label htmlFor="user-name" className="block font-medium text-black text-base mb-2">User Name</label>
                                <input type="text" name="user-name" placeholder="Enter User Name" autoComplete="user-name" className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-full py-2 px-5 placeholder-black placeholder-opacity-60 font-semibold" />
                            </div>
                            <div className="mb-5">
                                <label htmlFor="password" className="block font-medium text-black text-base mb-2">Password</label>
                                <input type="password" name="password" placeholder="Enter Password" autoComplete="password" className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-full py-2 px-5 placeholder-black placeholder-opacity-60 font-semibold" />
                            </div>
                            <div className="flex justify-end mb-2">
                                <Link href="/forgot-password">
                                    <a className="font-medium text-sm mb-2" style={{color:"$clr-theme"}}>Forgot Password?</a>
                                </Link>
                            </div>

                            <div className="flex justify-center mb-5">
                                <Link href="/signup">
                                <a className="bs-btn typ-medium text-white font-semibold text-center py-2.5 rounded-full">Log In</a>
                                </Link>
                            </div>
                            <div className="text-center">
                                <p className="font-semibold" style={{color:"rgba(0,0,0,0.6)"}}>Dont have an account? <Link href="/signup"><a style={{color:"$clr-theme"}}>Signup</a></Link></p>
                            </div>
                        </form>
                    </div>
                </TabPanel> 
                <TabPanel>
                    <div className="bs-form">
                        <form>
                        <div className="mb-5">
                            <label htmlFor="invite-code" className="block font-medium text-black text-base mb-2">Invite Code</label>
                            <input type="text" name="invite-code" placeholder="Enter Invite Code" autoComplete="invite-code" className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-full py-2 px-4 placeholder-black placeholder-opacity-60 font-semibold" />
                        </div>
                        <div className="grid md:grid-cols-2 md:gap-6 gap-0">
                            <div className="mb-5">
                                <label htmlFor="first-name" className="block font-medium text-black text-base mb-2">First Name</label>
                                <input type="text" name="first-name" placeholder="Enter First Name" autoComplete="given-name" className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-full py-2 px-4 placeholder-black placeholder-opacity-60 font-semibold" />
                            </div>
                            <div className="mb-5">
                                <label htmlFor="lirst-name" className="block font-medium text-black text-base mb-2">Lirst Name</label>
                                <input type="text" name="lirst-name" placeholder="Enter Last Name" autoComplete="given-name" className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-full py-2 px-4 placeholder-black placeholder-opacity-60 font-semibold" />
                            </div>
                        </div>
                        <div className="mb-5">
                            <label htmlFor="user-name" className="block font-medium text-black text-base mb-2">User Name</label>
                            <input type="text" name="user-name" placeholder="Enter User Name" autoComplete="user-name" className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-full py-2 px-4 placeholder-black placeholder-opacity-60 font-semibold" />
                        </div>
                        <div className="mb-5">
                            <label htmlFor="password" className="block font-medium text-black text-base mb-2">Password</label>
                            <input type="password" name="password" placeholder="Enter Password" autoComplete="password" className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-full py-2 px-4 placeholder-black placeholder-opacity-60 font-semibold" />
                        </div>
                        <div className="flex justify-end mb-2">
                            <Link href="/forgot-password">
                                <a className="font-medium text-sm mb-2" style={{color:"$clr-theme"}}>Forgot Password?</a>
                            </Link>
                        </div>
                        <div className="mb-8">
                            <input type="checkbox" className="form-checkbox" />
                            <span className="font-semibold ml-2" style={{color:"rgba(0,0,0,0.6)"}}>I agree to the Terms of Service and Privacy Policy</span>
                        </div>
                        <div className="flex justify-center mb-5">
                            <Link href="/">
                            <a className="bs-btn typ-medium text-white font-semibold text-center py-2.5 rounded-full">Log In</a>
                            </Link>
                        </div>
                        <div className="text-center">
                            <p className="font-semibold" style={{color:"rgba(0,0,0,0.6)"}}>Already have an account? <Link href="/"><a style={{color:"$clr-theme"}}>Log In</a></Link></p>
                        </div>
                        </form>
                    </div>
                </TabPanel>
                
             </Tabs>
        </div>
        )
    }
    export default TabsComponent