import Link from 'next/link';
import Image from 'next/image';
import logo from '../../../public/buzzencer_logo.svg'

function FooterComponent() {    
    return (       
        <div className="relative pt-14 overflow-hidden">
            <div className="bs-footer">
                <div className="main-footer py-7   border-b border-gray-300 bg-gray">
                    <div className="container mx-auto px-4 xl:px-0">
                        <div className="grid md:grid-cols-4 gap-8">
                            <div>
                                <Link href='/' passHref>
                                    <Image src={logo} alt="Buzzencer" />
                                </Link>
                                <p className="text-bg-darkgray mt-3.5 leading-loose">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s,</p>
                            </div>
                            <div className="md:pl-20">
                                {/* <h2 className="h-11 mb-5 flex items-center text-black font-semibold text-lg">Quick links</h2>
                                <Link href="/" passHref><p className="mb-4 text-bg-darkgray cursor-pointer">Faq</p></Link>
                                <Link href="/" passHref><p className="mb-4 text-bg-darkgray cursor-pointer">Sitemap</p></Link>
                                <Link href="/" passHref><p className="text-bg-darkgray cursor-pointer">T & C</p></Link> */}
                                <h2 className="h-11 mb-5 flex items-center text-black font-semibold text-lg">Contact Us</h2>
                                <address className="mb-4 text-bg-darkgray leading-loose not-italic md:text-sm md:w-32">Narmada Fresh Fruit Exports Flat No.4, Prashant Appt, Nashik - 422 009, Maharashtra</address>
                                <div className="mb-2">
                                    <a href="tel:9218540407">+91 9218540407</a>
                                </div>
                                <div>
                                    <a href="mailto:sanjay@narmadafresh.com">sanjay@narmadafresh.com</a>
                                </div>
                            </div>
                            <div className="md:pl-20">
                            <h2 className="h-11 mb-5 flex items-center text-black font-semibold text-lg">Quick links</h2>
                                <Link href="/" passHref><p className="mb-4 text-bg-darkgray cursor-pointer">Faq</p></Link>
                                <Link href="/" passHref><p className="mb-4 text-bg-darkgray cursor-pointer">Sitemap</p></Link>
                                <Link href="/" passHref><p className="text-bg-darkgray cursor-pointer">T & C</p></Link>
                            </div>
                            <div className="md:pl-12">
                                <h2  className="h-11 mb-5 flex items-center text-black font-semibold text-lg">Follow Us On</h2>
                                <div className="social-icn flex align-middle">
                                    <Link href="/" passHref><i className="icon icon-Icon-feather-facebook text-xl cursor-pointer"></i></Link>
                                    <Link href="/" passHref><i className="icon icon-Icon-ionic-logo-instagram ml-5 text-xl cursor-pointer"></i></Link>
                                    <Link href="/" passHref><i className="icon icon-Icon-awesome-twitter ml-5 text-xl cursor-pointer"></i></Link>
                                    <Link href="/" passHref><i className="icon icon-Icon-feather-linkedin ml-5 text-xl cursor-pointer"></i></Link>
                                </div>
                            </div>
                        </div>
                    </div>  
                </div>
                <div className="copyrights py-3.5 bg-gray">
                    <div className="container mx-auto px-4 xl:px-0">
                        <div className="grid md:grid-cols-2">
                            <div>
                                <p className="text-black md:text-left text-center mb-2 md:mb-0">© 2021 Buzzencers. All Rights Reserved.  </p>
                            </div>
                            <div>
                                <p className="flex md:justify-end justify-center text-black devlp-by">Developed By  <Link href="https://creativewebo.com/" passHref><a target="_blank"> Creative<span>Webo</span></a></Link></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>   
        </div> 
         
    )
}
export default FooterComponent