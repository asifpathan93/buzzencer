import { useState } from "react";

export default function Accordion(props) {
  const [isShowing, setIsShowing] = useState(false);

  const toggle = () => {
    setIsShowing(!isShowing);
  };

  return (
    <div className="border-2 mb-3" style={{borderRadius:'8px',borderColor:"gray"}}>
      <button
        onClick={toggle}
        type="button"
        className="p-3  w-full text-left flex justify-between items-center"
      >
        <p className="font-semibold text-black">{props.title}</p>
        <i className="icon icon-arrow"></i>
      </button>
      <div className="font-light text-black text-sm p-3 pt-0"
        style={{ display: isShowing ? "block" : "none" }}
        dangerouslySetInnerHTML={{
          __html: props.content
        }}
      />
    </div>
  );
}
