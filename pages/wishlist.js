import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link';
import HeaderComponent from "../components/common/Header/influencerheader";
import React from 'react';
import Modal from 'react-modal';

const customStyles = {
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
      minWidth:'30%',
      borderRadius:'7px'
    },
  };

    function Wishlist() {    
        let subtitle;
        const [modalIsOpen, setIsOpen] = React.useState(false);
  
        function openModal() {
            setIsOpen(true);
    }
  
    function afterOpenModal() {
      // references are now sync'd and can be accessed.
    //   subtitle.style.color = '#f00';
    }
  
    function closeModal() {
      setIsOpen(false);
    }
  return (
     <div>
        <Head>
          <title>Buzzencers</title>
          <meta name="description" content="Generated by create next app" />
          <link rel="icon" href="/fav.ico" />
        </Head>
        {/* modelpopup start */}

        <Modal
        isOpen={modalIsOpen}
        onAfterOpen={afterOpenModal}
        onRequestClose={closeModal}
        contentLabel="Example Modal"
        style={customStyles}>
            <div className="flex justify-between mb-3">
                <h2 className="font-semibold text-black text-sm">Add your new Brand!</h2>
                <button onClick={closeModal}><i className="icon icon-Cancel text-2xl font-medium"></i></button>
            </div>
            <div className="bs-form">
                <div className="from-group mb-5">
                    <input type="password" name="old-password" placeholder="Enter your Brand" autoComplete="old-password" className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-full py-2 px-5 placeholder-black placeholder-opacity-60" />
                </div>
                <div className="flex justify-center">
                    <Link href="/">
                        <a className="bs-btn text-white font-semibold text-center py-2.5 rounded-full">Submit</a>
                    </Link>
                </div>
            </div>
            
        </Modal>
        {/* modelpopup End */}
        <div>
            <section className="lyt-section-dash">
                <div className="flex flex-row"> 
                    <div className="xl:w-1/4  small_header">
                        <HeaderComponent/>
                    </div>
                    <div className="xl:w-11/12 2xl:-ml-5 p-5 xl:pl-9 mt-16 bg-gray  w-full wishDash">
                        <div className="bs-card typ-fullcard typ-Dashboard">
                            <div className="grid md:grid-cols-2 item-center mb-5">
                                <div>
                                    <div className="relative flex items-center md:w-3/5">
                                        <i className="icon icon-search-1 absolute left-5 top-3.5" style={{color:"rgba(0,0,0,0.5)"}}></i>
                                        <input type="text" name="user-name" placeholder="search your brand" autoComplete="user-name" className="pl-10 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm font-medium border-gray-300 rounded-full py-2 px-5 placeholder-black placeholder-opacity-60" />
                                    </div>
                                </div>
                                <div className="flex md:justify-end md:mt-0 mt-4">
                                    {/* <Link href="/"> */}
                                        <a className="bs-btn text-white font-semibold text-center py-2.5 rounded-full" onClick={openModal}>Add New</a>
                                    {/* </Link> */}
                                </div>
                            </div>

                            <div>
                                <div className="grid lg:grid-cols-5 md:grid-cols-4 grid-cols-2 gap-5">
                                    <div>
                                        <div className="bs-card p-4">
                                            <div className="flex justify-between">
                                                <p className="leading-none font-medium text-sm text-black">Puma</p>
                                                <input type="checkbox" className="form-checkbox w-3.5 h-3.5" checked  style={{borderRadius:"3px"}}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="bs-card p-4">
                                            <div className="flex justify-between">
                                                <p className="leading-none font-medium text-sm text-black">Reebok</p>
                                                 <input type="checkbox" className="form-checkbox w-3.5 h-3.5"  style={{borderRadius:"3px"}}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="bs-card p-4">
                                            <div className="flex justify-between">
                                                <p className="leading-none font-medium text-sm text-black">Nike</p>
                                                 <input type="checkbox" className="form-checkbox w-3.5 h-3.5"  style={{borderRadius:"3px"}}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="bs-card p-4">
                                            <div className="flex justify-between">
                                                <p className="leading-none font-medium text-sm text-black">Cat</p>
                                                 <input type="checkbox" className="form-checkbox w-3.5 h-3.5"  style={{borderRadius:"3px"}}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="bs-card p-4">
                                            <div className="flex justify-between">
                                                <p className="leading-none font-medium text-sm text-black">Reebok</p>
                                                 <input type="checkbox" className="form-checkbox w-3.5 h-3.5"  style={{borderRadius:"3px"}}/>
                                            </div>
                                        </div>
                                    </div>
                                   
                                    <div>
                                        <div className="bs-card p-4">
                                            <div className="flex justify-between">
                                                <p className="leading-none font-medium text-sm text-black">Parle-G</p>
                                                 <input type="checkbox" className="form-checkbox w-3.5 h-3.5"  style={{borderRadius:"3px"}}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="bs-card p-4">
                                            <div className="flex justify-between">
                                                <p className="leading-none font-medium text-sm text-black">Samsung</p>
                                                 <input type="checkbox" className="form-checkbox w-3.5 h-3.5"  style={{borderRadius:"3px"}}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="bs-card p-4">
                                            <div className="flex justify-between">
                                                <p className="leading-none font-medium text-sm text-black">Nokia</p>
                                                 <input type="checkbox" className="form-checkbox w-3.5 h-3.5"  style={{borderRadius:"3px"}}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="bs-card p-4">
                                            <div className="flex justify-between">
                                                <p className="leading-none font-medium text-sm text-black">Vivo</p>
                                                 <input type="checkbox" className="form-checkbox w-3.5 h-3.5"  style={{borderRadius:"3px"}}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="bs-card p-4">
                                            <div className="flex justify-between">
                                                <p className="leading-none font-medium text-sm text-black">Samsung</p>
                                                <input type="checkbox" className="form-checkbox w-3.5 h-3.5"  style={{borderRadius:"3px"}}/>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div>
                                        <div className="bs-card p-4">
                                            <div className="flex justify-between">
                                                <p className="leading-none font-medium text-sm text-black">Windows 7</p>
                                                <input type="checkbox" className="form-checkbox w-3.5 h-3.5"  style={{borderRadius:"3px"}}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="bs-card p-4">
                                            <div className="flex justify-between">
                                                <p className="leading-none font-medium text-sm text-black">Hindustan Uniliver</p>
                                                 <input type="checkbox" className="form-checkbox w-3.5 h-3.5"  style={{borderRadius:"3px"}}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="bs-card p-4">
                                            <div className="flex justify-between">
                                                <p className="leading-none font-medium text-sm text-black">Coca-Cola</p>
                                                 <input type="checkbox" className="form-checkbox w-3.5 h-3.5"  style={{borderRadius:"3px"}}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="bs-card p-4">
                                            <div className="flex justify-between">
                                                <p className="leading-none font-medium text-sm text-black">Microsoft</p>
                                                 <input type="checkbox" className="form-checkbox w-3.5 h-3.5"  style={{borderRadius:"3px"}}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="bs-card p-4">
                                            <div className="flex justify-between">
                                                <p className="leading-none font-medium text-sm text-black">Mercedes-Benz</p>
                                                 <input type="checkbox" className="form-checkbox w-3.5 h-3.5"  style={{borderRadius:"3px"}}/>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div>
                                        <div className="bs-card p-4">
                                            <div className="flex justify-between">
                                                <p className="leading-none font-medium text-sm text-black">American Express</p>
                                                 <input type="checkbox" className="form-checkbox w-3.5 h-3.5"  style={{borderRadius:"3px"}}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="bs-card p-4">
                                            <div className="flex justify-between">
                                                <p className="leading-none font-medium text-sm text-black">J.P. Morgan</p>
                                                 <input type="checkbox" className="form-checkbox w-3.5 h-3.5"  style={{borderRadius:"3px"}}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="bs-card p-4">
                                            <div className="flex justify-between">
                                                <p className="leading-none font-medium text-sm text-black">Accenture</p>
                                                 <input type="checkbox" className="form-checkbox w-3.5 h-3.5"  style={{borderRadius:"3px"}}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="bs-card p-4">
                                            <div className="flex justify-between">
                                                <p className="leading-none font-medium text-sm text-black">Volkswagen</p>
                                                 <input type="checkbox" className="form-checkbox w-3.5 h-3.5"  style={{borderRadius:"3px"}}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="bs-card p-4">
                                            <div className="flex justify-between">
                                                <p className="leading-none font-medium text-sm text-black">L&apos;Oréal</p>
                                                <input type="checkbox" className="form-checkbox w-3.5 h-3.5"  style={{borderRadius:"3px"}}/>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div>
                                        <div className="bs-card p-4">
                                            <div className="flex justify-between">
                                                <p className="leading-none font-medium text-sm text-black">Goldman Sachs</p>
                                                <input type="checkbox" className="form-checkbox w-3.5 h-3.5"  style={{borderRadius:"3px"}}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="bs-card p-4">
                                            <div className="flex justify-between">
                                                <p className="leading-none font-medium text-sm text-black">Starbucks</p>
                                                 <input type="checkbox" className="form-checkbox w-3.5 h-3.5"  style={{borderRadius:"3px"}}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="bs-card p-4">
                                            <div className="flex justify-between">
                                                <p className="leading-none font-medium text-sm text-black">Morgan Stanley</p>
                                                 <input type="checkbox" className="form-checkbox w-3.5 h-3.5"  style={{borderRadius:"3px"}}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="bs-card p-4">
                                            <div className="flex justify-between">
                                                <p className="leading-none font-medium text-sm text-black">Thomson Reuters</p>
                                                 <input type="checkbox" className="form-checkbox w-3.5 h-3.5"  style={{borderRadius:"3px"}}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="bs-card p-4">
                                            <div className="flex justify-between">
                                                <p className="leading-none font-medium text-sm text-black">Panasonic</p>
                                                <input type="checkbox" className="form-checkbox w-3.5 h-3.5"  style={{borderRadius:"3px"}}/>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div>
                                        <div className="bs-card p-4">
                                            <div className="flex justify-between">
                                                <p className="leading-none font-medium text-sm text-black">Tiffany & Co.</p>
                                                 <input type="checkbox" className="form-checkbox w-3.5 h-3.5"  style={{borderRadius:"3px"}}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="bs-card p-4">
                                            <div className="flex justify-between">
                                                <p className="leading-none font-medium text-sm text-black">MasterCard</p>
                                                 <input type="checkbox" className="form-checkbox w-3.5 h-3.5"  style={{borderRadius:"3px"}}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="bs-card p-4">
                                            <div className="flex justify-between">
                                                <p className="leading-none font-medium text-sm text-black">Land Rover</p>
                                                 <input type="checkbox" className="form-checkbox w-3.5 h-3.5"  style={{borderRadius:"3px"}}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="bs-card p-4">
                                            <div className="flex justify-between">
                                                <p className="leading-none font-medium text-sm text-black">Harley-Davidson</p>
                                                 <input type="checkbox" className="form-checkbox w-3.5 h-3.5"  style={{borderRadius:"3px"}}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div className="bs-card p-4">
                                            <div className="flex justify-between">
                                                <p className="leading-none font-medium text-sm text-black">Jack Daniel&apos;s</p>
                                                <input type="checkbox" className="form-checkbox w-3.5 h-3.5"  style={{borderRadius:"3px"}}/>
                                            </div>
                                        </div>
                                    </div>
                                
                                </div>
                                <div className="action-btns flex justify-center mt-8">
                                    <div className="flex items-center mr-10">
                                        <Link href='/dashboard' passHref>
                                            <a className="bs-btn typ-white font-semibold flex justify-center item-center py-2.5 rounded-full">Clear</a>
                                        </Link>
                                    </div>
                                    <div className="flex items-center">
                                        <Link href='/campaign-apply' passHref>
                                            <a className="bs-btn font-semibold text-white flex justify-center item-center py-2.5 rounded-full">Submit</a>
                                        </Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>    
                </div>
            </section>
        </div>
    </div>        
  )
}
export default Wishlist
