import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link';
import HeaderComponent from "../../components/common/Header/adminheader";
import React from 'react';
import customerpic from "../../public/customer.png";
 function CompleteApplicationDetail() {               
  return (
      <div>
        <Head>
          <title>Buzzencers</title>
          <meta name="description" content="Generated by create next app" />
          <link rel="icon" href="/fav.ico" />
        </Head>
     
        <section>
            <div className="flex flex-row"> 
                <div className="xl:w-1/4">
                    <HeaderComponent/>
                </div>
                <div className="xl:w-11/12 2xl:-ml-8 mt-16 bg-gray w-full h-screen">
                    <div className="px-5 md:pl-9">
                        <div className="bs-card typ-fullcard typ-Dashboard mt-5">
                            <div className="md:flex items-center justify-between mb-8 pt-3">
                                    <div className="flex items-center">
                                        <Image src={customerpic} alt=""/>
                                        <p className="text-base text-black font-medium ml-5">Glory Methri</p>
                                    </div>
                                    <div className="md:flex items-center md:mt-0 mt-4">
                                        <div className="relative mr-5 md:-mt-2">
                                                <div className="absolute w-full sm:text-sm border-gray-300 border rounded-full py-2 px-5 placeholder-black placeholder-opacity-60 cursor-pointer bg-white">
                                                    <div className="flex flex-row items-center"> 
                                                        <span className="block text-gray-400 font-normal">Upload Invoice here..</span>
                                                        <i className="icon-upload absolute right-5" style={{color:'#c0c0c1'}}></i>
                                                    </div>
                                                </div> 
                                                <input type="file" className="h-full w-full opacity-0 cursor-pointer " name=""/>
                                        </div>
                                        <div className="flex md:mt-0 mt-6">
                                            <Link href='/' passHref>
                                                <a className="bs-btn font-semibold  text-white text-center py-2.5 rounded-full">Pay</a>
                                            </Link>
                                        </div>
                                    </div>
                            </div>
                            <p className="text-base font-semibold mb-5">Bank Details</p>
                            <div className="grid md:grid-cols-3 gap-6 mb-8">
                                <div>
                                    <p className="text-black font-medium text-sm mb-1">Bank Name</p>
                                    <p className="text-black font-normal text-xs" style={{color:"rgba(0,0,0,0.6)"}}>HDFC Bank</p>
                                </div>
                                <div>
                                    <p className="text-black font-medium text-sm mb-1">IFSC Code</p>
                                    <p className="text-black font-normal text-xs" style={{color:"rgba(0,0,0,0.6)"}}>263265</p>
                                </div>
                                <div>
                                    <p className="text-black font-medium text-sm mb-1">Account Number</p>
                                    <p className="text-black font-normal text-xs" style={{color:"rgba(0,0,0,0.6)"}}>125426789623267</p>
                                </div>
                                <div>
                                    <p className="text-black font-medium text-sm mb-1">Account Holder Name</p>
                                    <p className="text-black font-normal text-xs" style={{color:"rgba(0,0,0,0.6)"}}>Prasad Ramkrishnan Desai</p>
                                </div>
                                <div>
                                    <p className="text-black font-medium text-sm mb-1">Bank Type:</p>
                                    <p className="text-black font-normal text-xs" style={{color:"rgba(0,0,0,0.6)"}}>Saving</p>
                                </div>
                            </div>

                            <p className="text-base font-semibold mb-5">Personal Details</p>
                            <div className="grid md:grid-cols-4 gap-6">
                                <div>
                                    <p className="text-black font-medium text-sm mb-1">Phone No</p>
                                    <p className="text-black font-normal text-xs" style={{color:"rgba(0,0,0,0.6)"}}>+919833130890</p>
                                </div>
                                <div>
                                    <p className="text-black font-medium text-sm mb-1">Country</p>
                                    <p className="text-black font-normal text-xs" style={{color:"rgba(0,0,0,0.6)"}}>India</p>
                                </div>
                                <div>
                                    <p className="text-black font-medium text-sm mb-1">Gender</p>
                                    <p className="text-black font-normal text-xs" style={{color:"rgba(0,0,0,0.6)"}}>Female</p>
                                </div>
                                <div>
                                    <p className="text-black font-medium text-sm mb-1">Pincode</p>
                                    <p className="text-black font-normal text-xs" style={{color:"rgba(0,0,0,0.6)"}}>400102</p>
                                </div>
                                <div>
                                    <p className="text-black font-medium text-sm mb-1">Email id</p>
                                    <p className="text-black font-normal text-xs" style={{color:"rgba(0,0,0,0.6)"}}>glorymethri@gmail.com</p>
                                </div>
                                <div>
                                    <p className="text-black font-medium text-sm mb-1">State</p>
                                    <p className="text-black font-normal text-xs" style={{color:"rgba(0,0,0,0.6)"}}>Maharashtra</p>
                                </div>
                                <div>
                                    <p className="text-black font-medium text-sm mb-1">City</p>
                                    <p className="text-black font-normal text-xs" style={{color:"rgba(0,0,0,0.6)"}}>Mumbai</p>
                                </div>
                                <div>
                                    <p className="text-black font-medium text-sm mb-1">Date Of Birth</p>
                                    <p className="text-black font-normal text-xs" style={{color:"rgba(0,0,0,0.6)"}}>06 march 1998</p>
                                </div>
                            </div>
                            <div className="mt-4">
                                <p className="text-black font-medium text-sm mb-1">Address</p>
                                <p className="text-black font-normal text-xs" style={{color:"rgba(0,0,0,0.6)"}}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&apos;s standard dummy text ever since the 1500s, when  an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                            </div>
                        </div>
                    </div>
                  
                </div>
            </div>
        </section> 
      </div>
       
  )
 }
 export default CompleteApplicationDetail