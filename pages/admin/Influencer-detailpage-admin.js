import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link';
import HeaderComponent from "../../components/common/Header/adminheader";
import customerpic from "../../public/customer.png";

function Dashboard() {
    return (
        <div>

            <Head>
                <title>Buzzencers</title>
                <meta name="description" content="Generated by create next app" />
                <link rel="icon" href="/fav.ico" />
            </Head>

            <div>
                <section>
                    <div className="flex flex-row">
                        <div className="xl:w-1/4">
                            <HeaderComponent />
                        </div>
                        <div className="xl:w-11/12 mt-16 p-5 xl:pl-9 2xl:-ml-8  w-full h-full">
                            <div className="grid md:grid-cols-3 gap-4 mb-4">
                                <div>
                                    <div className="bs-card typ-fullcard relative clr-cards typ-four">
                                        <p className="text-base text-black mb-2 font-medium">Buzzencer Level</p>
                                        <div className="flex items-center">
                                            <p className="w-2/4 text-2xl font-semibold">12</p>
                                            <div className="w-2/4 flex justify-end">
                                                <div className="w-12 h-12 rounded-full flex items-center justify-center text-white"
                                                    style={{ backgroundColor: "#C21F1F" }}><span
                                                        className="icon-Level text-xl"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div className="bs-card typ-fullcard relative clr-cards typ-five">
                                        <p className="text-base text-black mb-2 font-medium">Buzzencer Score</p>
                                        <div className="flex items-center">
                                            <p className="w-2/4 text-2xl font-semibold">100+</p>
                                            <div className="w-2/4 flex justify-end">
                                                <div className="w-12 h-12 rounded-full flex items-center justify-center text-white"
                                                    style={{ backgroundColor: "#1AA2AC" }}><span
                                                        className="icon-Star text-xl"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="grid md:grid-cols-a gap-6">
                                {/* <div className="bs-card">
                            sdfgh
                        </div>
                        <div className="bs-card">
                            sdfgh
                        </div> */}
                                <div className="flex" style={{ width: "100%" }}>
                                    <div className="pr-5 pb-5" style={{ width: "70%" }} >
                                        <div className="bs-card typ-fullcard mt-5 px-5 py-5">
                                            <div className="md:flex items-center justify-between mb-8">
                                                <div className="flex items-center">
                                                    <Image src={customerpic} alt="" />
                                                    <p className="text-base text-black font-medium ml-5">Glory Methri</p>
                                                </div>
                                                <div className="md:flex items-center md:mt-0 mt-4">
                                                    <div className="flex md:mt-0 mt-6 mr-4">
                                                        <Link href='/' passHref>
                                                            <a
                                                                className="bs-btn typ-white font-semibold  text-white text-center py-2.5 rounded-full">Select</a>
                                                        </Link>
                                                    </div>
                                                    <div className="flex md:mt-0 mt-6">
                                                        <Link href='/' passHref>
                                                            <a
                                                                className="bs-btn font-semibold  text-white text-center py-2.5 rounded-full"><i
                                                                    className="icon icon-phone-call-01 pr-2"></i>Call</a>
                                                        </Link>
                                                    </div>
                                                </div>
                                            </div>

                                            <p className="text-base font-semibold mb-5">Personal Details</p>
                                            <div className="grid md:grid-cols-4 gap-6">
                                                <div>
                                                    <p className="text-black font-medium text-sm mb-1">Phone No</p>
                                                    <p className="text-black font-normal text-xs" style={{
                                                        color: "rgba(0,0,0,0.6)"
                                                    }}>+919833130890</p>
                                                </div>
                                                <div>
                                                    <p className="text-black font-medium text-sm mb-1">Country</p>
                                                    <p className="text-black font-normal text-xs" style={{
                                                        color: "rgba(0,0,0,0.6)"
                                                    }}>India</p>
                                                </div>
                                                <div>
                                                    <p className="text-black font-medium text-sm mb-1">Gender</p>
                                                    <p className="text-black font-normal text-xs" style={{
                                                        color: "rgba(0,0,0,0.6)"
                                                    }}>Female</p>
                                                </div>
                                                <div>
                                                    <p className="text-black font-medium text-sm mb-1">Pincode</p>
                                                    <p className="text-black font-normal text-xs" style={{
                                                        color: "rgba(0,0,0,0.6)"
                                                    }}>400102</p>
                                                </div>
                                                <div>
                                                    <p className="text-black font-medium text-sm mb-1">Email id</p>
                                                    <p className="text-black font-normal text-xs" style={{
                                                        color: "rgba(0,0,0,0.6)"
                                                    }}>glorymethri@gmail.com</p>
                                                </div>
                                                <div>
                                                    <p className="text-black font-medium text-sm mb-1">State</p>
                                                    <p className="text-black font-normal text-xs" style={{
                                                        color: "rgba(0,0,0,0.6)"
                                                    }}>Maharashtra</p>
                                                </div>
                                                <div>
                                                    <p className="text-black font-medium text-sm mb-1">City</p>
                                                    <p className="text-black font-normal text-xs" style={{
                                                        color: "rgba(0,0,0,0.6)"
                                                    }}>Mumbai</p>
                                                </div>
                                                <div>
                                                    <p className="text-black font-medium text-sm mb-1">Date Of Birth</p>
                                                    <p className="text-black font-normal text-xs" style={{
                                                        color: "rgba(0,0,0,0.6)"
                                                    }}>06 march 1998</p>
                                                </div>
                                            </div>
                                            <div className="mt-4">
                                                <p className="text-black font-medium text-sm mb-1">Address</p>
                                                <p className="text-black font-normal text-xs" style={{
                                                    color: "rgba(0,0,0,0.6)"
                                                }}>Lorem Ipsum is simply dummy text of the printing and typesetting
                                                    industry. Lorem Ipsum has been the industry&apos;s standard dummy text ever since
                                                    the 1500s, when an unknown printer took a galley of type and scrambled it to
                                                    make a type specimen book.</p>
                                            </div>
                                        </div>
                                        <div className="bs-card typ-fullcard mt-5 px-5 py-5">
                                            <div className="md:flex items-center justify-between mb-4">
                                                <div className="flex items-center">
                                                    <p className="text-base text-black font-semibold">Social Media</p>
                                                </div>
                                            </div>
                                            <div className="grid md:grid-cols-2 gap-6">
                                                <div className="flex items-center">
                                                    <i className="icon-Path-37434 text-4xl rounded-full mr-4 text-white"></i>
                                                    <Link href="https://www.instagram.com/buzzencer/" passHref>
                                                        <a className="break-all">https://www.instagram.com/buzzencer/</a>
                                                    </Link>
                                                </div>
                                                <div className="flex items-center">
                                                    <i
                                                        className="icon-Icon-ionic-logo-instagram text-2xl p-2 rounded-full mr-4 text-white instagram-logo"></i>
                                                    <Link href="https://www.instagram.com/buzzencer/" passHref>
                                                        <a className="break-all">https://www.instagram.com/buzzencer/</a>
                                                    </Link>
                                                </div>
                                                <div className="flex items-center">
                                                    <i
                                                        className="icon-Icon-ionic-logo-instagram text-2xl p-2 rounded-full mr-4 text-white instagram-logo"></i>
                                                 <Link href="https://www.instagram.com/buzzencer/" passHref>
                                                        <a className="break-all">https://www.instagram.com/buzzencer/</a>
                                                    </Link>
                                                </div>
                                                <div className="flex items-center">
                                                    <i
                                                        className="icon-Icon-ionic-logo-instagram text-2xl p-2 rounded-full mr-4 text-white instagram-logo"></i>
                                                    <Link href="https://www.instagram.com/buzzencer/" passHref>
                                                        <a className="break-all">https://www.instagram.com/buzzencer/</a>
                                                    </Link>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="bs-card typ-fullcard mt-5 px-5 py-5">
                                            <div className="md:flex items-center justify-between mb-4">
                                                <div className="flex items-center">
                                                    <p className="text-base text-black font-semibold">Interest</p>
                                                </div>
                                            </div>
                                            <div className="grid md:grid-cols-3 gap-6">
                                                <div>
                                                    <p className="font-medium pb-2">Primary Interest: </p>
                                                    <p className="cm-grey">Business & Startup </p>
                                                </div>
                                                <div>
                                                    <p className="font-medium pb-2">Secondary Interest:</p>
                                                    <p className="cm-grey pb-2">Beauty & Make Up</p>
                                                    <p className="cm-grey pb-2">Automobile</p>
                                                    <p className="cm-grey">Business & Startup</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="bs-card typ-fullcard mt-5 px-5 py-5">
                                            <div className="md:flex items-center justify-between mb-4">
                                                <div className="flex items-center">
                                                    <p className="text-base text-black font-semibold">Bank Details</p>
                                                </div>
                                            </div>
                                            <div className="grid md:grid-cols-3 gap-6">
                                                <div>
                                                    <p className="font-medium pb-2">Bank Name</p>
                                                    <p className="cm-grey">HDFC Bank </p>
                                                </div>
                                                <div>
                                                    <p className="font-medium pb-2">IFSC Code</p>
                                                    <p className="cm-grey pb-2">263265</p>
                                                </div>
                                            </div>
                                            <div className="grid md:grid-cols-3 gap-6 pt-4">
                                                <div>
                                                    <p className="font-medium pb-2">Account Holder Name</p>
                                                    <p className="cm-grey">Glory Methri</p>
                                                </div>
                                                <div>
                                                    <p className="font-medium pb-2">bank Type</p>
                                                    <p className="cm-grey pb-2">Saving</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="mt-5" style={{ width: "30%" }}>
                                        <div className="bs-card typ-fullcard px-5 py-5">
                                            <div className="flex py-2 justify-between border-bottom">
                                                <p className="font-bold">
                                                    29 Influencers
                                                </p>
                                                <p className="font-bold">
                                                    View All
                                                </p>
                                            </div>
                                            <ul className="overflow-y-scroll" style={{ height: "400px" }}>
                                                <li className="py-2 border-bottom">
                                                    <p className="font-bold">
                                                        Kiran Raut
                                                    </p>
                                                    <p style={{
                                                        color: "#4B4B4B"
                                                    }}>
                                                        Mumbai, Maharashtra
                                                    </p>
                                                    <p style={{
                                                        color: "#717171"
                                                    }}>
                                                        Reviewed
                                                    </p>
                                                </li>

                                                <li className="py-2 border-bottom">
                                                    <p className="font-bold">
                                                        Kiran Raut
                                                    </p>
                                                    <p style={{
                                                        color: "#4B4B4B"
                                                    }}>
                                                        Mumbai, Maharashtra
                                                    </p>
                                                    <p style={{
                                                        color: "#717171"
                                                    }}>
                                                        Reviewed
                                                    </p>
                                                </li>

                                                <li className="py-2 border-bottom">
                                                    <p className="font-bold">
                                                        Kiran Raut
                                                    </p>
                                                    <p style={{
                                                        color: "#4B4B4B"
                                                    }}>
                                                        Mumbai, Maharashtra
                                                    </p>
                                                    <p style={{
                                                        color: "#717171"
                                                    }}>
                                                        Reviewed
                                                    </p>
                                                </li><li className="py-2 border-bottom">
                                                    <p className="font-bold">
                                                        Kiran Raut
                                                    </p>
                                                    <p style={{
                                                        color: "#4B4B4B"
                                                    }}>
                                                        Mumbai, Maharashtra
                                                    </p>
                                                    <p style={{
                                                        color: "#717171"
                                                    }}>
                                                        Reviewed
                                                    </p>
                                                </li>

                                                <li className="py-2 border-bottom">
                                                    <p className="font-bold">
                                                        Kiran Raut
                                                    </p>
                                                    <p style={{
                                                        color: "#4B4B4B"
                                                    }}>
                                                        Mumbai, Maharashtra
                                                    </p>
                                                    <p style={{
                                                        color: "#717171"
                                                    }}>
                                                        Reviewed
                                                    </p>
                                                </li><li className="py-2 border-bottom">
                                                    <p className="font-bold">
                                                        Kiran Raut
                                                    </p>
                                                    <p style={{
                                                        color: "#4B4B4B"
                                                    }}>
                                                        Mumbai, Maharashtra
                                                    </p>
                                                    <p style={{
                                                        color: "#717171"
                                                    }}>
                                                        Reviewed
                                                    </p>
                                                </li>
                                                <li className="py-2 border-bottom">
                                                    <p className="font-bold">
                                                        Kiran Raut
                                                    </p>
                                                    <p style={{
                                                        color: "#4B4B4B"
                                                    }}>
                                                        Mumbai, Maharashtra
                                                    </p>
                                                    <p style={{
                                                        color: "#717171"
                                                    }}>
                                                        Reviewed
                                                    </p>
                                                </li>
                                                <li className="py-2 border-bottom">
                                                    <p className="font-bold">
                                                        Kiran Raut
                                                    </p>
                                                    <p style={{
                                                        color: "#4B4B4B"
                                                    }}>
                                                        Mumbai, Maharashtra
                                                    </p>
                                                    <p style={{
                                                        color: "#717171"
                                                    }}>
                                                        Reviewed
                                                    </p>
                                                </li>
                                                <li className="py-2 border-bottom">
                                                    <p className="font-bold">
                                                        Kiran Raut
                                                    </p>
                                                    <p style={{
                                                        color: "#4B4B4B"
                                                    }}>
                                                        Mumbai, Maharashtra
                                                    </p>
                                                    <p style={{
                                                        color: "#717171"
                                                    }}>
                                                        Reviewed
                                                    </p>
                                                </li>
                                                <li className="py-2 border-bottom">
                                                    <p className="font-bold">
                                                        Kiran Raut
                                                    </p>
                                                    <p style={{
                                                        color: "#4B4B4B"
                                                    }}>
                                                        Mumbai, Maharashtra
                                                    </p>
                                                    <p style={{
                                                        color: "#717171"
                                                    }}>
                                                        Reviewed
                                                    </p>
                                                </li>
                                                <li className="py-2 border-bottom">
                                                    <p className="font-bold">
                                                        Kiran Raut
                                                    </p>
                                                    <p style={{
                                                        color: "#4B4B4B"
                                                    }}>
                                                        Mumbai, Maharashtra
                                                    </p>
                                                    <p style={{
                                                        color: "#717171"
                                                    }}>
                                                        Reviewed
                                                    </p>
                                                </li>
                                                <li className="py-2 border-bottom">
                                                    <p className="font-bold">
                                                        Kiran Raut
                                                    </p>
                                                    <p style={{
                                                        color: "#4B4B4B"
                                                    }}>
                                                        Mumbai, Maharashtra
                                                    </p>
                                                    <p style={{
                                                        color: "#717171"
                                                    }}>
                                                        Reviewed
                                                    </p>
                                                </li>
                                                <li className="py-2 border-bottom">
                                                    <p className="font-bold">
                                                        Kiran Raut
                                                    </p>
                                                    <p style={{
                                                        color: "#4B4B4B"
                                                    }}>
                                                        Mumbai, Maharashtra
                                                    </p>
                                                    <p style={{
                                                        color: "#717171"
                                                    }}>
                                                        Reviewed
                                                    </p>
                                                </li>
                                                <li className="py-2 border-bottom">
                                                    <p className="font-bold">
                                                        Kiran Raut
                                                    </p>
                                                    <p style={{
                                                        color: "#4B4B4B"
                                                    }}>
                                                        Mumbai, Maharashtra
                                                    </p>
                                                    <p style={{
                                                        color: "#717171"
                                                    }}>
                                                        Reviewed
                                                    </p>
                                                </li>
                                                <li className="py-2 border-bottom">
                                                    <p className="font-bold">
                                                        Kiran Raut
                                                    </p>
                                                    <p style={{
                                                        color: "#4B4B4B"
                                                    }}>
                                                        Mumbai, Maharashtra
                                                    </p>
                                                    <p style={{
                                                        color: "#717171"
                                                    }}>
                                                        Reviewed
                                                    </p>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    {/* <div className="posts">
                {content}
            </div>

            <ReactPaginate previousLabel={'previous'} nextLabel={'next'} breakLabel={'...'} breakClassName={'break-me'}
                activeClassName={'active'} containerClassName={'pagination'} subContainerClassName={'pages pagination'}
                initialPage={props.currentPage - 1} pageCount={props.pageCount} marginPagesDisplayed={2}
                pageRangeDisplayed={5} onPageChange={pagginationHandler} /> */}
                </section>
            </div>
        </div>
    )
}

//Fetching posts in get Intial Props to make the app seo friendly
// Dashboard.getInitialProps = async ({ query }) => {
// const page = query.page || 1; //if page empty we request the first page
// const posts = await
//
// axios.get(`https://gorest.co.in/public-api/posts?_format=json&access-token=cxzNs8fYiyxlk708IHfveKM1z1xxYZw99fYE&page=1`);
// return {
// totalCount: posts.data._meta.totalCount,
// pageCount: posts.data._meta.pageCount,
// currentPage: posts.data._meta.currentPage,
// perPage: posts.data._meta.perPage,
// };

// }
export default Dashboard