import React from 'react'
import { Bar } from 'react-chartjs-2'
import { Chart as ChartJS } from 'chart.js/auto'
import { Chart }            from 'react-chartjs-2'


const Chart1 = () => {
    
    return (
        <div>
            <Bar
                data={{
                    labels: ['May','Jun','Jul','Aug','Sep'],
                    datasets:[
                        {
                            
                            data:[18,8,12,22,30],
                            backgroundColor:[
                                '#403BBF',
                            ],
                            borderRadius: 6,
                            barThickness:18
                        }
                    ]
                }}
                height={400}
                width={550}
                options={{
                    maintainAspectRatio:false,
                    scales:{
                        x:{
                            beginAtZero:true,
                            grid:{
                                display:false 
                            }
                        },
                        
                    },
                    layout: {
                        padding: 20,
                    },
                    plugins:{
                        legend:{
                            display:false
                        }
                    }
                }}
            />
        </div>
    )
}

export default Chart1