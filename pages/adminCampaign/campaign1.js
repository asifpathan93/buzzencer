import Head from 'next/head'
import Image from 'next/image'
import Link from 'next/link';
import AdminHeader from '../../components/common/Header/adminheader';
import React,{useState,useEffect,useRef} from 'react';
import camppic from "../../public/campimg.png";
import campvideo from "../../public/campvideo.png";
import insta from "../../public/instagram.svg";

function Campaign1() {
    const [showDropdown, setShowDropdown] = useState(false);

    const dropdown = useRef(null);

    useEffect(() => {
        // only add the event listener when the dropdown is opened
        if (!showDropdown) return;
        function handleClick(event) {
          if (dropdown.current && !dropdown.current.contains(event.target)) {
            setShowDropdown(false);
          }
        }
        window.addEventListener("click", handleClick);
        // clean up
        return () => window.removeEventListener("click", handleClick);
      }, [showDropdown]);
    
    return (
        <div>
            <Head>
            <title>Buzzencers</title>
            <meta name="description" content="Generated by create next app" />
            <link rel="icon" href="/fav.ico" />
            </Head>
        
            <section className="lyt-section-dash">
                    
                <div className="xl:w-11/18  w-full h-auto">
                    <div className="p-5 md:pl-7 px-4">
                        {/* steper step one start */}
                        <div className=" bs-card typ-fullcard typ-Dashboard typ-campaigndetail">
                            <form className="p-2">
                                <div className="grid md:grid-cols-3 gap-6">
                                    <div>
                                        <label className="block font-medium text-black text-sm mb-2">Brand Name</label>
                                        <input type="text" name="user-name" placeholder="Enter name" autoComplete="user-name" className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-full py-2 px-5 placeholder-black placeholder-opacity-60" />
                                    </div>

                                    <div>
                                        <label  className="block font-medium text-black text-sm mb-2">Campaign Title</label>
                                        <input type="text" name="phone-no" placeholder="Enter title" autoComplete="phone-no" className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-full py-2 px-5 placeholder-black placeholder-opacity-60" />
                                    </div>

                                    <div>
                                        <label  className="block font-medium text-black text-sm mb-2">Campaign Budget</label>
                                        <input type="email" name="email-id" placeholder="Enter budget" autoComplete="email-id" className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-full py-2 px-5 placeholder-black placeholder-opacity-60" />
                                    </div>
                                </div>
                                <div className='grid md:grid-cols-2  gap-6'>
                                    <div className="mt-6">
                                        <label  className="block font-medium text-black text-sm mb-2">Campaign Description</label>
                                        <textarea placeholder="Write proper address.." className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 py-2 px-4 placeholder-black placeholder-opacity-60" rows="8" style={{borderRadius:"10px"}}></textarea>
                                    </div>
                                    <div className='flex flex-col mt-7'>
                                        <div className="relative">
                                            <label className="block font-medium text-black text-sm mb-2">Platforms</label>
                                            <select className="block appearance-none mt-1 focus:ring-indigo-500 focus:border-indigo-500 w-full shadow-sm sm:text-sm border-gray-300 py-2 px-4 placeholder-black placeholder-opacity-60 bg-transparent rounded-full" id="grid-state" style={{color:"rgba(0,0,0,0.6)"}}>
                                                <option>Select platform</option>
                                                <option>Male</option>
                                                <option>Female</option>
                                                <option>Transgender</option>
                                            </select>
                                        </div>
                                        <div>
                                            <label  className="block font-medium text-black text-sm mt-2 mb-2">Campaign Start Date</label>
                                            <input type="date" name="startdate" placeholder="Select Start Date" autoComplete="start-date" className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-full py-2 px-5 placeholder-black placeholder-opacity-60" />
                                        </div>
                                    <div>
                                        <label  className="block font-medium text-black text-sm mt-2 mb-2">Campaign End Date</label>
                                        <input type="date" name="email-id" placeholder="Select Platform" autoComplete="email-id" className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-full py-2 px-5 placeholder-black placeholder-opacity-60" />
                                    </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div className="flex md:justify-start btn_sec justify-start mt-5">
                            <Link href='/adminCampaignlist/list2' passHref>
                                <a className="bs-btn text-white text-center py-2.5 rounded-full">Add Task</a>
                            </Link>
                        </div>
                    </div>

                    <div className="p-5 md:pl-7 px-4">
                        {/* steper step one start */}
                        <div className="bs-card typ-fullcard typ-Dashboard typ-campaigndetail">
                            <form className="p-5">
                                <div className='grid md:grid-cols-2 gap-6'>
                                    
                                    <div className='flex flex-col mt-7'>
                                        <div>
                                            <label  className="block font-medium text-black text-sm mb-2">Task Name</label>
                                            <input type="text" name="user-name" placeholder="Enter name" autoComplete="user-name" className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-full py-2 px-5 placeholder-black placeholder-opacity-60" />
                                        </div>
                                        <div className='mt-5'>
                                            <label  className="block font-medium text-black text-sm mb-5">Demo Material</label>
                                            <input type="text" name="user-name" placeholder="Upload video" autoComplete="user-name" className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-full py-2 px-5 placeholder-black placeholder-opacity-60" />
                                        </div>
                                        <div>
                                            <input type="text" name="user-name" placeholder="Upload image" autoComplete="user-name" className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-full py-2 px-5 placeholder-black placeholder-opacity-60" />
                                        </div>
                                        <div>
                                            
                                            <input type="text" name="email-id" placeholder="Add link" autoComplete="email-id" className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-full py-2 px-5 placeholder-black placeholder-opacity-60" />
                                        </div>
                                    </div>
                                    <div className="mt-6">
                                        <label className="block font-medium text-black text-sm mb-2">Task Description</label>
                                        <textarea placeholder="Enter description" className="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 py-2 px-4 placeholder-black placeholder-opacity-60" rows="8" style={{borderRadius:"10px"}}></textarea>
                                        <div className="flex md:justify-end justify-end mt-5">
                                            <Link href='/adminCampaignlist/list2' passHref>
                                                <a className="border px-12 text-center py-2.5 rounded-full mr-10 bg-white text-blue-600 font-medium">Cancel</a>
                                            </Link>
                                            <Link href='/adminCampaignlist/list2' passHref>
                                                <a className="bs-btn text-white text-center py-2.5 rounded-full font-medium">Save</a>
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div> 
                    <div className="p-5 md:pl-7 px-4">
                        {/* <div className="bs-card typ-fullcard typ-Dashboard typ-campaigndetail mb-20">
                                
                                    
                        </div> */}
                        <div className="deliverables-info typ-campaigndetail bg-white rounded-2xl p-4">
                            <div className='flex justify-between'>
                            <h3 className="text-base text-black mb-1 font-semibold">1) Instagram IGTV with our brand product.</h3>
                            <div>
                                        <span className="icon-Dots" onClick={() => setShowDropdown(b => !b)}></span>
                                        {showDropdown && (
                                            <div ref={dropdown} className="absolute right-12 mt-2 rounded-xl shadow p-7 bg-white profile-dropdwns">
                                                <ul>
                                                    <li className="mb-4">
                                                        <Link href="/reset-password-admin" passHref>
                                                            <a className="text-black font-medium">Edit</a>
                                                        </Link>
                                                    </li>
                                                    <li>
                                                        <Link href="/" passHref>
                                                            <a className="text-black font-medium">Delete</a>
                                                        </Link>
                                                    </li>
                                                </ul>
                                            </div>
                                        )}
                            </div>
                            </div>
                            <p className="text-sm leading-6 mb-3" style={{color:'rgba(0,0,0,0.6)'}}>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&apos;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&apos;s standard dummy text ever since the 1500s, when an unknown.</p>
                            <p className="text-sm text-black mb-3 font-semibold">Demo Material</p>
                            
                            <div className="md:flex">
                                <div className="md:mr-16">
                                    <p className="text-sm text-black mb-2 font-semibold">Videos</p>
                                        <Link href="/" passHref>
                                            <a className="relative flex md:justify-center items-center">
                                                <Image src={campvideo} alt="Campaign detail"/>
                                                <i className="icon icon-play-circle-icon absolute lg:left-20 md:left-10 flex items-center justify-center left-24 text-3xl" style={{color:"#FEB224"}}></i>
                                            </a>
                                        </Link>
                                </div>
                                <div className="md:mr-16">
                                    <p className="text-sm text-black mb-2 font-semibold">Images</p>
                                    <Image src={camppic} alt="Campaign detail"/>
                                </div>
                                <div>
                                    <p className="text-sm text-black mb-2 font-semibold">Social links</p>
                                    <p className="text-sm text-black mb-2 font-regular">Instagram:</p>
                                    <Link href="https://www.instagram.com/p/CRWiGw8Iukt/?utm_source=ig_web_copy_link" passHref>
                                        <a className="font-semibold text-sm mb-2 block break-words">https://www.instagram.com/p/CRWiGw8Iukt/?utm_source=ig_web_copy_link</a>
                                    </Link>
                                    <p className="text-sm text-black my-2 font-regular">Facebook:</p>
                                    <Link href="https://www.instagram.com/p/CRWiGw8Iukt/?" passHref>
                                        <a className="font-semibold text-sm mb-2 block break-words">utm_source=ig_web_copy_link</a>
                                    </Link>
                                </div>
                            </div>
                        </div>

                        <div className="mt-5 typ-campaigndetail deliverables-info bg-white rounded-2xl p-4">
                        <div className='flex justify-between'>
                            <h3 className="text-base text-black mb-1 font-semibold">1) Instagram IGTV with our brand product.</h3>
                            <div>
                                <span className="icon-Dots" onClick={() => setShowDropdown(b => !b)}></span>
                                {showDropdown && (
                                <div ref={dropdown} className="absolute right-12 mt-2 rounded-xl shadow p-7 bg-white profile-dropdwns">
                                        <ul>
                                            <li className="mb-4">
                                                <Link href="/reset-password-admin" passHref>
                                                    <a className="text-black font-medium">Edit</a>
                                                </Link>
                                            </li>
                                            <li>
                                                <Link href="/" passHref>
                                                    <a className="text-black font-medium">Delete</a>
                                                </Link>
                                            </li>
                                        </ul>
                                    </div>
                                    )}
                                </div>
                            </div>
                            <p className="text-sm leading-6 mb-3" style={{color:'rgba(0,0,0,0.6)'}}>is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&apos;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&apos;s standard dummy text ever since the 1500s, when an unknown.</p>
                            <p className="text-sm text-black mb-3 font-semibold">Demo Material</p>
                            <div className="md:flex">
                                <div className="md:mr-16">
                                    <p className="text-sm text-black mb-2 font-semibold">Videos</p>
                                        <Link href="/" passHref>
                                            <a className="relative flex md:justify-center items-center">
                                                <Image src={campvideo} alt="Campaign detail"/>
                                                <i className="icon icon-play-circle-icon absolute lg:left-20 md:left-10 flex items-center justify-center left-24 text-3xl" style={{color:"#FEB224"}}></i>
                                            </a>
                                        </Link>
                                </div>
                                <div className="md:mr-16">
                                    <p className="text-sm text-black mb-2 font-semibold">Images</p>
                                    <Image src={camppic} alt="Campaign detail"/>
                                </div>
                                <div>
                                    <p className="text-sm text-black mb-2 font-semibold">Social links</p>
                                    <p className="text-sm text-black mb-2 font-regular">Instagram:</p>
                                    <Link href="https://www.instagram.com/p/CRWiGw8Iukt/?utm_source=ig_web_copy_link" passHref>
                                        <a className="font-semibold text-sm mb-2 block break-words">https://www.instagram.com/p/CRWiGw8Iukt/?utm_source=ig_web_copy_link</a>
                                    </Link>
                                    <p className="text-sm text-black my-2 font-regular">Facebook:</p>
                                    <Link href="https://www.instagram.com/p/CRWiGw8Iukt/?" passHref>
                                        <a className="font-semibold text-sm mb-2 block break-words">utm_source=ig_web_copy_link</a>
                                    </Link>
                                </div>
                            </div>
                        </div>
                                    
                    </div>      
                </div>
            </section> 
        </div>
    )
}
 export default Campaign1